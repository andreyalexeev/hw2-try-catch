'use strict';

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  
  const rootElement = document.getElementById("root");
  const ulElement = document.createElement("ul");
  
  books.forEach(book => {
    if (book.author && book.name && book.price) {
      const liElement = document.createElement("li");
      liElement.textContent = `${book.name} (${book.author}) - $${book.price}`;
      ulElement.appendChild(liElement);
    } else {
      console.error(`Помилка: в об'єкті відсутня одна з властивостей: author, name або price.`);
    }
  });
  
  rootElement.appendChild(ulElement);
  